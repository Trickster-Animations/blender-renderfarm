"""
Module containing functions to discover Blender Frameservers and other utility functions
"""
import ipaddress
import socket
from threading import Thread
from time import time

from pickle import load, dump, UnpicklingError
from requests import get
from requests.exceptions import Timeout
from .utils import construct_sck


def discover_nodes():
    """
    Attempts to discover all running Blender Framserver nodes on the local network.

    :return: List of IPs of active nodes.
    :rtype: list
    """
    nodes = []
    sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sck.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sck.bind(("", 3558))
    sck.settimeout(10)
    now = time()
    while time()-now < 10:
        try:
            data, addr = sck.recvfrom(1024)
        except socket.timeout:
            break
        if data.startswith(b"BR_NOTIFY") and (addr[0], int(data.split(b" ")[1]), int(data.split(b" ")[2])) not in nodes:
            print("Discovered new node:", addr[0], int(data.split(b" ")[1]), int(data.split(b" ")[2]))
            nodes.append((addr[0], int(data.split(b" ")[1]), int(data.split(b" ")[2])))
            now = time()
    return nodes