import ffmpeg
from os.path import join as pjoin
from os import listdir

def encode_frames(directory:str, output:str, framerate:int, seqlen:int):
    frames = ffmpeg.input(pjoin(directory, f"%0{seqlen}d.png"), **{"pattern_type": "sequence"})
    frames = ffmpeg.filter(frames, "fps", fps=framerate)
    frames = ffmpeg.output(frames, output, vcodec="qtrle")
    frames = ffmpeg.overwrite_output(frames)
    ffmpeg.run(frames, capture_stdout=True, capture_stderr=True)

    