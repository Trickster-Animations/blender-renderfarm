"""
Module containing functions to collect rendered frames from the render nodes.
"""
import logging
from json import dumps, loads
from os import rmdir
from os.path import join as pjoin
from threading import Thread
import requests
from time import time

from tqdm import tqdm

from .utils import get_info


def _frame_collector(node:tuple, start:int, end:int, step:int, offset:int, directory:str, progress:tqdm):
    for frame in range(start+offset, end+1, step):
        with open(pjoin(directory, f"{frame:0{len(str(end))}d}.png"), "wb") as framefile:
            framefile.write(requests.get(f"http://{node[0]}:{node[2]}/image/{frame:0{len(str(end))}d}.png").content)
        progress.update()
    requests.get(f"http://{node[0]}:{node[2]}/close.txt")
    progress.write(f"All frames collected from {node}.")


def _profiling_frame_collector(node:tuple, start:int, end:int, progress:tqdm, timings:dict):
    t_start = time()
    for frame in range(start, end+1):
        requests.get(f"http://{node[0]}:{node[2]}/image/{frame:0{len(str(end))}d}.png").content
        progress.update()
    t_end = time()
    timings[node] = (end-start+1)/(t_end - t_start)
    requests.get(f"http://{node[0]}:{node[2]}/close.txt")
    progress.write(f"All frames collected from {node}.")


def benchmark_nodes(nodes:list, timings:dict, render_info:dict):
    progress = tqdm(
        desc="Collecting frames...",
        total=(render_info["end"]-render_info["start"]+1)*len(nodes),
        unit="frame"
    )
    threads = [Thread(target=_profiling_frame_collector, args=(
        node,
        render_info["start"],
        render_info["end"],
        progress,
        timings
    )) for node in set(nodes)]
    for thread in threads: thread.start()
    for thread in threads: thread.join()
    progress.close()
    print()
    return timings


def collect_frames(nodes:list, directory:str, render_info:dict, timings:None):
    progress = tqdm(
        desc="Collecting frames...",
        total=(render_info["end"]-render_info["start"])+1,
        unit="frame") 
    threads = [Thread(target=_frame_collector, args=(
        node, 
        render_info["start"],
        render_info["end"],
        len(nodes),
        nodes.index(node),
        directory,
        progress)) for node in set(nodes)]
    for thread in threads: thread.start()
    for thread in threads: thread.join()
    progress.close()
    print()


def _calculate_assigned_frames(framecounts):
    assigned = 0
    for node in framecounts:
        assigned = assigned + framecounts[node]
    return assigned


def _optimized_frame_collector(node:tuple, start:int, framecount:int, directory:str, progress:tqdm):
    for frame in range(start, start+framecount):
        with open(pjoin(directory, f"{frame:0{len(str(start+framecount))}d}.png"), "wb") as framefile:
            framefile.write(requests.get(f"http://{node[0]}:{node[2]}/image/{frame:0{len(str(start+framecount))}d}.png").content)
        progress.update()
    requests.get(f"http://{node[0]}:{node[2]}/close.txt")
    progress.write(f"All frames collected from {node}.")


def optimized_collector(nodes:list, directory:str, render_info:dict, timings:dict):
    total_frames = render_info["end"] - render_info["start"] + 1
    total_fps = 0
    for node in nodes:
        total_fps = total_fps + timings[node]
    frames_per_fps = total_frames/total_fps
    frame_counts = {}
    for node in nodes:
        frame_counts[node] = int(frames_per_fps * timings[node])
    assigned_frames = _calculate_assigned_frames(frame_counts)
    if assigned_frames < total_frames:
        best_node = None
        best_framecount = 0
        for node in nodes:
            if frame_counts[node] > best_framecount:
                best_node = node
                best_framecount = frame_counts[node]
        frame_counts[best_node] = frame_counts[best_node] + (total_frames-assigned_frames)
    assigned_frames = _calculate_assigned_frames(frame_counts)
    if assigned_frames > total_frames:
        best_node = None
        best_framecount = 0
        for node in nodes:
            if frame_counts[node] > best_framecount:
                best_node = node
                best_framecount = frame_counts[node]
        frame_counts[best_node] = frame_counts[best_node] - (assigned_frames-total_frames)
    assert _calculate_assigned_frames(frame_counts) == total_frames
    start_frames = {}
    start_frames[nodes[0]] = render_info["start"]
    for i in range(1, len(nodes)):
        start_frames[nodes[i]] = start_frames[nodes[i-1]] + frame_counts[nodes[i-1]]
    print("Frame assignments:")
    print("Start frame:", render_info["start"])
    for node in nodes:
        print("Node:", node, "\n\tStart:", start_frames[node], "\n\tEnd:", start_frames[node]+frame_counts[node]-1)
    print("End frame:", render_info["end"])
    progress = tqdm(
        desc="Collecting frames...",
        total=(render_info["end"]-render_info["start"])+1,
        unit="frame") 
    threads = [Thread(target=_optimized_frame_collector, args=(
        node, 
        start_frames[node],
        frame_counts[node],
        directory,
        progress)) for node in set(nodes)]
    for thread in threads: thread.start()
    for thread in threads: thread.join()
    progress.close()
    print()
