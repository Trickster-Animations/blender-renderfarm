import socket
from threading import Thread

from tqdm import tqdm

def _send(node:str, filepath:str):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sck:
        sck.connect(node[0:2])
        with open(filepath, "rb") as blendfile:
            sck.sendall(blendfile.read())
        sck.recv(1024)


def _send_data(node:str, filedata:bytes):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sck:
        sck.connect(node[0:2])
        sck.sendall(filedata)
        sck.recv(1024)


def send_to_nodes(nodes:list, filepath:str=None, filedata:bytes=None):
    if filepath is not None:
        threads = [Thread(target=_send, args=(node,), kwargs={"filepath":filepath}) for node in nodes]
    elif filedata is not None:
        threads = [Thread(target=_send_data, args=(node,), kwargs={"filedata":filedata}) for node in nodes]
    else:
        raise TypeError("Must specify either filepath or filedata.")
    for thread in threads: thread.start()
    for thread in tqdm(
        threads,
        "Sending data to nodes...",
        unit="nodes"
        ): thread.join()
    print()