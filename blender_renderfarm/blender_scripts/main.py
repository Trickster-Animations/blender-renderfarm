import os
import socket
from tempfile import NamedTemporaryFile, mkdtemp
from shutil import rmtree
from time import sleep

from threading import Thread

import bpy
from bpy.app.handlers import persistent
from http.server import HTTPServer, BaseHTTPRequestHandler


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/info.txt":
            data = {
                "rate": bpy.data.scenes['Scene'].render.fps,
                "start": bpy.data.scenes['Scene'].frame_start,
                "end": bpy.data.scenes['Scene'].frame_end
            }
            self.send_response(200)
            self.send_header("Content-Type", "text/plain")
            self.end_headers()
            for key, value in data.items():
                self.wfile.write(bytes(f"{key} {value}\n", "UTF-8"))
            return
        if self.path == "/close.txt":
            self.send_response(200)
            self.send_header("Content-Type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes("Server stopping now...", "UTF-8"))
            Thread(target=self.server.shutdown).start()
            return
        if self.path.startswith("/image/") and os.path.split(self.path)[1].lower().endswith(".png"):
            image = os.path.split(self.path)[1]
            frame = int(image.split(".")[0])
            outpath = bpy.data.scenes['Scene'].render.filepath
            bpy.data.scenes['Scene'].frame_set(frame)
            bpy.ops.render.render(write_still=True)
            self.send_response(200)
            self.send_header("Content-Type", "image/png")
            self.end_headers()
            with open(outpath, "rb") as imagefile:
                self.wfile.write(imagefile.read())
            return
        self.send_response(404)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write(b"That... doesn't exists tho...")


class Notifier():
    def __init__(self):
        self.sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sck.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self._thread = None

    def start(self):
        self.active = True
        self._thread = Thread(target=self._run)
        self._thread.start()

    def stop(self):
        self.active = False
        if self._thread is not None:
            self._thread.join()
            self._thread = None

    def _run(self):
        while self.active:
            self.sck.sendto(b"BR_NOTIFY %s %s" % (bytes(os.getenv("CPU_PORT", "3558"), "utf-8"), bytes(os.getenv("FRAMESERVER_PORT", "8080"), "utf-8")), ("<broadcast>", 3558))
            sleep(3)

    def close(self):
        self.active = False
        self._thread.join()
        self.sck.close()


@persistent
def handle_render():
    notifier = Notifier()
    while True:  
        notifier.start()
        print("Waiting for a new job...")
        with NamedTemporaryFile(delete=False, suffix=".blend") as datafile:
            waiting = True
            while waiting:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.bind((os.getenv("CPU_IP", "0.0.0.0"), int(os.getenv("CPU_PORT", 3558))))
                    s.listen(0)
                    conn, addr = s.accept()
                    print("Receiving blend file from {}".format(addr))
                    while True:
                        data = conn.recv(1024)
                        if data == b"STOP":
                            conn.sendall(b"ACK")
                            bpy.ops.wm.quit_blender()
                            return
                        if len(data) < 1024:
                            waiting = False
                            datafile.write(data)
                            break
                        datafile.write(data)
        datafile.close()
        notifier.stop()
        bpy.ops.wm.open_mainfile(filepath=datafile.name)
        bpy.data.scenes['Scene'].render.image_settings.file_format = 'PNG'
        bpy.data.scenes['Scene'].cycles.device = 'CPU'  
        bpy.data.scenes['Scene'].render.filepath = os.path.join(mkdtemp(), "currentframe.png")
        with HTTPServer(("", int(os.getenv("FRAMESERVER_PORT", 8080))), Handler) as httpd:
            print("Starting frameserver.")
            conn.sendall(b"RDY")
            conn.close()
            httpd.serve_forever()
        os.remove(datafile.name)
        rmtree(os.path.split(bpy.data.scenes['Scene'].render.filepath)[0])

handle_render()
