from sys import exit

from .server_discovery import discover_nodes 
from .send_blend import send_to_nodes
from .collector import collect_frames, optimized_collector, benchmark_nodes
from .encoder import encode_frames
from .utils import get_info, info_wrapper, construct_sck

__version__ = '0.1.0'


def construct_farmspec():
    from tempfile import TemporaryDirectory
    from pkgutil import get_data
    from pickle import dump

    print("Preparing for benchmark.")
    benchfile = get_data("blender_renderfarm", "resources/bench.blend")
    nodes = discover_nodes()
    if len(nodes) == 0:
        print("Cannot construct farmspec with no nodes available.")
        exit(1)
    send_to_nodes(nodes, filedata=benchfile)
    info = get_info(nodes[0])
    timing_data = {}
    benchmark_nodes(nodes, timing_data, info)
    with open("farmspec", "wb") as farmspec:
        dump(timing_data, farmspec)


def render(blendfile=None, outfile=None):
    from tempfile import TemporaryDirectory
    from tkinter import filedialog
    from tkinter import Tk, TclError
    from pickle import load
    
    try:
        Tk().withdraw()
        if blendfile is None:
            blendfile = filedialog.askopenfilename(filetypes=(("BLEND files", "*.blend"),("Old BLEND files", "*.blend1")))
        if outfile is None:
            outfile = filedialog.asksaveasfilename(filetypes=(("MOV files", "*.mov"),))
    except TclError:
        if blendfile is None and outfile is None:
            print("Can't initialize GUI and no input and/or output file provided.")
            exit()

    try:
        with open("farmspec", "rb") as farmspec:
            timing_data = load(farmspec)
        render_proj = optimized_collector
    except IOError:
        print("Farmspec not available. Frame splits won't be optimized.")
        timing_data = None
        render_proj = collect_frames

    print("Discovering nodes.")
    nodes = discover_nodes()
    if len(nodes) == 0:
        print("No nodes available.")
        exit()
    print()
    send_to_nodes(nodes, blendfile)
    info = get_info(nodes[0])
    with TemporaryDirectory() as render_dir:
        render_proj(nodes, render_dir, info, timing_data)
        encode_frames(render_dir, outfile, info["rate"], len(str(info["end"])))
        print("Done!")