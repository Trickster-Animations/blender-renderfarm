import fire


class Application():
    def render(self, infile=None, outfile=None):
        try:
            from . import render
        except ImportError:
            from blender_renderfarm import render
        render(infile, outfile)
    
    def benchmark(self):
        try:
            from . import construct_farmspec
        except ImportError:
            from blender_renderfarm import construct_farmspec
        construct_farmspec()


fire.Fire(Application)